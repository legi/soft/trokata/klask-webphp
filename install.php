<?
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>blue:sky - welcome</title>
    
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    
    <meta name="generator" content="Jonas John" />
    <meta name="description" content="put a good description in here" />
    <meta name="keywords" content="good,keywords" />
    
    <link rel="stylesheet" type="text/css" href="css/bluesky.css" media="screen, tv, projection" title="Default" />
    <link rel="alternative stylesheet" type="text/css" href="css/bluesky_large.css" media="screen, projection, tv" title="Larger Fonts" />
    
    <link rel="alternative stylesheet" type="text/css" href="css/print.css" media="screen" title="Print Preview" />
    <link rel="alternative stylesheet" type="text/css" href="css/handheld.css" media="screen" title="Small Layout Preview" />
    
    <link rel="stylesheet" type="text/css" href="css/handheld.css" media="handheld" title="Small Layout" />
    <link rel="stylesheet" type="text/css" href="css/print.css" media="print" />

    <!-- Navigational metadata (an accessibility feature - preview in opera) -->
    <link rel="top" href="index.html" title="Homepage" />
    <link rel="up" href="index.html" title="Up" />
    <link rel="first" href="index.html" title="First page" />
    <link rel="previous" href="index.html" title="Previous page" />
    <link rel="next" href="index.html" title="Next page" />
    <link rel="last" href="index.html" title="Last page" />
    <link rel="toc" href="index.html" title="Table of contents" />
    <link rel="index" href="index.html" title="Site map" />
	
</head>
<body>

<div id="page">
    
    <div id="header">
        <a href="index.html">ENS LYON</a>
    </div>
    
    <div id="wrapper"> 
        
        <div id="content">
        
            <div id="path">
                A NICE GUI FOR KLASK
            </div>
    
            <div id="main">

                <!-- <h1>Welcome !</h1> -->
					
                <p>
                  <?
					
					include('./install-inc.php');
				
					?>
                </p>
                
            </div>

        </div>
        
	</div>
    
    <div id="footer">
        <p>
             License GPL V2 <a href="index.html">WebKlask</a>.
            Webdesign disponible sur <a href="http://www.oswd.org">Open Source Web Design</a>  by <a href="http://www.jonasjohn.de/">Jonas John</a>.
            
            <!-- please leave a small credit to the author - thanks :-) ! -->
            
        </p>
    </div>

</div>

</body>
</html>
