<?
require('./libUser.php');
require('./libService.php');
require('./libRight.php');
require('./libSwitch.php');
require('./libDevice.php');
require('./libNetwork.php');
require('./libKlask.php');
if ($GLOBALS['auth'] == "ldap") {
	require('./ldap/auth_ldap.php');
	require('./libLDAP.php');
}

/***************************************************************************************
displayAuthUser()
displayAddUser()
displayResultAdduser($idLogin, $passwdUser, $service)
displayDelUser()
displayResultDelUser($idLogin)
displayAddSwitch()
displayResultAddSwitch($idSwitch, $ip, $mac, $type, $location, $level, $portIgnore, $service)
displayDelSwitch()
displayResultDelSwitch($idSwitch) 
displayUpdateSwitch($idSwitch)
displayResultUpdateSwitch($idSwitch, $ip, $mac, $type, $location, $level, $portIgnore)
displayAllSwitch()
displayAddService()
displayResultAddService($idService) 
displayDelService()
displayResultDelService($service)
displayAllService() 
displayAddPower()
displayResultAddPower($idPower, $descPower)
displayDelPower()
displayResultDelPower($power) 
displayAddUserPower() 
displayResultAddUserPower($user, $power)
displayDelUserPower()
displayResultDelUserPower($user, $power)
displayResultDelUserPowerBydisplayAllPower($userPower)
displayPowerUser($idLogin)
displayPowerService($idService) 
displayAddPowerService()
displayResultAddPowerService($power, $service)
displayResultDelUserPowerBydisplayAllPower($servicePower)
displayAddUserService()
displayResultAddUserService($user, $service)
displayAddSwitchService() 
displayResultAddSwitchService($switch, $service)
displayAddNetwork()
displayAllNetwork()
displayResultAddNetwork($addressNetwork, $nameNetwork, $interface,  $routeur) 
displayDelNetwork()
displayResultDelNetwork($network) 
displayAllDevice()
displayAddDevice()
displayResultAddDevice($hostname, $mac, $ip,  $type)
displayDelDevice() 
displayResultDelDevice($device)

	
****************************************************************************************/

function contentdefault() {
	$buffer = "<h1>WEB KLASK</h1>";
	$buffer .= "De très nombreux outils réseaux existent mais la plupart permettent de tracer des cartographies de réseau basées sur la notion de route. Au niveau d'un réseau local commuté, cette notion de route n'apporte rien et ce genre d'outil n'est en général pas capable de structurer la carte locale des machines. L'administrateur d'un site de taille moyenne, quelques bâtiments, une quinzaine de commutateurs, se retrouvent rapidement devant les problématiques suivantes :<BR /><BR />";
	$buffer .="- Où est positionnée la machine X ? Elle est à l'origine d'un problème réseau urgent à traiter, quitte à la déconnecter en désactivant le port du commutateur ;<BR />";
	$buffer .= "- Deux machines A et B de mon réseau local ne dialoguent pas. Quel est le chemin physique menant de la machine A vers la machine B ?<BR /><BR />";
	$buffer .= "Rapidement, lorsque son parc machine augmente, il devient difficile de maintenir une version papier à jour de son réseau local, notamment s'il y a des mouvements de personnel... Il est possible d'améliorer les choses en configurant le matériel actif de manière à n'associer certaines adresses physiques (MAC) de machine qu'à certains ports de commutateur.Cependant, cela ne résout pas forcément tous les problèmes, notamment le second.<BR /><BR />"; 
	
	$buffer .= "Klask est un petit outil, dans l'esprit des outils UNIX, de ne se préoccuper que des connections sur le réseau local. Cependant, Klask fonctionne sur un réseau comportant plusieurs classe réseau et plusieurs VLAN. Pour configurer Klask, consulter la page MultiVlan.<BR /><BR />";
	
//	$buffer .= "<b><a href=\"./install.php\">Pour lancer la procédure d'installation</a></b>";
	return $buffer ;
}



function navAdd() {
	
		$buffer  = "<a href=\"./index.php?section=addUser\">Utilisateur</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=addDevice\">Device</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=addService\">Service</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=addSwitch\">Switch</a><BR />";
//		$buffer  .= "<a href=\"./index.php?section=addPower\">Droits</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=addNetwork\">Réseau</a><BR />";
		
		$buffer  .= "<a href=\"./index.php?section=addUserService\">Service à un utilisateur</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=addSwitchService\">Service à un switch</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=addPowerUser\">Droits à un utilisateur</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=addPowerService\">Droits à un service</a><BR />";
		
		
		
	
//		$buffer  .= "<a href=\"./index.php?section=delPower\">Supprimer un droit ou commande</a><BR><BR>";
		return $buffer ;
	
}


function navDel() {
	
		$buffer  = "<a href=\"./index.php?section=delUser\">Supprimer un utilisateur</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=delService\">Supprimer un Service</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=delDevice\">Supprimer un Device</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=delSwitch\">Supprimer un Switch</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=delNetwork\">Supprimer un Réseau</a><BR />";
		return $buffer ;
	
}


function navDisplay() {
	
		$buffer  = "<a href=\"./index.php?section=allDevice\">Afficher les Devices</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=allNetwork\">Afficher les Réseaux</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=allService\">Afficher les services</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=allSwitch\">Afficher les switchs</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=powerUser\">Afficher les droits des utilisateurs</a><BR />";
		$buffer  .= "<a href=\"./index.php?section=powerService\">Afficher les droits des services</a><BR />";
		return $buffer ;
		
}


function navklask() {
	$buffer  = "<a href=\"./index.php?section=genKlaskConf\">Générer le fichier klask</a><BR />";
	$buffer  .= "<a href=\"./index.php?section=klaskExportdb\">Liste des machines</a><BR />";
	$buffer  .= "<a href=\"./index.php?section=klaskExportsw\">Liste des switch et de leurs liaisons</a><BR />";
	$buffer  .= "<a href=\"./index.php?section=klaskUpdatesw\">Mise à jour de lase base des Switch</a><BR />";
	$buffer  .= "<a href=\"./index.php?section=klaskUpdatedb\">Detection de machines</a><BR />";
	$buffer  .= "<a href=\"./index.php?section=klaskGenmap\">Générer la carte du réseau</a><BR />";
#	$buffer  .= "<a href=\"./index.php?section=klaskSearch\">Rechercher une machine sur le réseau</a><BR />";
	return $buffer ;
}


function displayAuthUser() {
	
	$buffer = "<FORM METHOD=\"POST\" ACTION=\"./index.php?section=checkAuth\">";
	$buffer .= "<TABLE id='login'>";
	$buffer .= "<TR><TD id='login'>Authentification</TD></TR>";
	$buffer .= "<TR><TD id='login'>Login</TD></TR>";
	$buffer .= "<TR><TD id='login'><INPUT id='login' TYPE=\"TEXT\" NAME=\"textIdLogin\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id='login'>Mot de passe</TD></TR>";
	$buffer .= "<TR><TD id='login'><INPUT id='login' TYPE=\"PASSWORD\" NAME=\"textPasswd\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id='login'><INPUT id='login' TYPE=\"SUBMIT\" VALUE=\"VALIDER\"></TD></TR>";
	$buffer .= "</TABLE></FORM>";
  
    return $buffer;  
		
}



function displayAddUser() {
	
	$buffer = "<h1>AJout d'un utlisateur</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addUser", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addUser\">";
	
	$buffer .= "<TABLE id=\"Add\">";
	
	$buffer .= "<TR><TD id=title>Login de l'uilisateur (ID): </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIdLogin\"></INPUT></TD></TR>";
	//TODO: attention si auth ldap, ne pas aller ici 
	
    if ( $GLOBALS['auth'] == "classic" ) {
		
		$buffer .= "<TR><TD id=title>Mot de passe: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textPasswdUser\"></INPUT></TD></TR>";
		
		$resultat = selectService("") ;
		//display for debugger
	    //echo "type de ressources: " .get_resource_type($resultat);
		if ( !is_string($resultat) ) {
			$buffer .= "<TR><TD id=title>Service</TD><TD><SELECT  NAME=\"selectIdService[]\" MULTIPLE SIZE=5>";
			while ( $row = mysql_fetch_assoc($resultat) ) {
				$buffer .= "<OPTION VALUE=\"".$row['idService']. "\">".$row['idService']."</OPTION>";
			}
			$buffer .= "</SELECT></TD></TR>";
		}else{
			$buffer .="<TD>$resultat</TD></TR>";
		}
		
	}
	
	$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" NAME=\"AJOUTER\"></INPUT></TD></TR></TABLE></FORM>";
	
	return $buffer;
	
}

function displayResultAdduser($idLogin, $passwdUser, $service) {
	
	$buffer = "<h1>Resultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addUser", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	
	$buffer .= "<TABLE>";
	$resultat = 0;
	$resultat = ctrlEmpty($idLogin,  "Login Utilisateur");
	if ( $resultat != 1 ){
		
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	if ( ctrlUnixAccount($idLogin) == 0 and $GLOBALS['auth'] == "ldap" ) {
	
	$buffer .= "<TR></TD>L'utilisateur n'existe pas dans l'annuaire</TD></TR>";
	$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
	return $buffer ;
	}

	
	//if auth classic, ctrl user's passwd
	 if ( $GLOBALS['auth'] == "classic" )  {
		$resultat = 0;
		$resultat = ctrlEmpty($passwdUser,  "Mot de passe");
		if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
		}
	}
	
	//if auth ldap get user's service
	 if ( $GLOBALS['auth'] == "ldap" )  {
		$service = getServiceLDAP($idLogin);
	}	
	$resultat = 0;
	$resultat = ctrlEmpty($service,  "Service");
	if ( $resultat != 1 ){
		
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addUser", "" );
	if ( $resultat != 1 ){
		
		$buffer .="<TR><TD>ACCESS DENIED FOR THIS COMMAND $idPower FOR ". $_SESSION['login'] ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
		
	}
	
	//TODO: to clear
	//echo "libDisplay.resultaddUser.php md5 saisie passe: " .  md5($passwdUser) . " et md5 adm " .md5("adm") . "<br>";
	$resultat = 0;
	$resultat = addUserDB($idLogin, $passwdUser);
	if ($resultat == 1 ) {
	   	$buffer .= "<TR><TD>CREATE USER $idLogin: [OK]</TD></TR>";
		
	}else{
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		return $buffer ."</TABLE>";
	}
	
	if ( $GLOBALS['auth'] == "ldap" )  {

		for ( $cpt=0;$cpt<$service['count'];$cpt++) {
		//foreach ( $service as $idService ) {
			//Verify if service exist. If service not exist then add service.
			//This function is only avalaible for ldap auth 
			$resultat = selectService($service[$cpt]) ;
			if (  !is_string($resultat) ) {
				$rowService = mysql_fetch_assoc($resultat) ;
				if ( $rowService['idService'] != $service[$cpt] ) {
					$resService = addService($service[$cpt]) ;
					if ($resService == 1 ) {
						$buffer .= "<TR><TD>ADD SERVICE $service[$cpt] : [OK]</TD></TR>";		
					}else{
						$buffer .= "<TR><TD>$resService</TD></TR>";
						$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
						return $buffer ."</TABLE>";
					}
				}
			}
			$resultat = addUserService($idLogin, $service[$cpt]);
			if ($resultat == 1 ) {
				$buffer .= "<TR><TD>ADD SERVICE $service[$cpt] FOR $idLogin: [OK]</TD></TR>";		
			}else{
				$buffer .= "<TR><TD>$resultat</TD></TR>";
				$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
				return $buffer ."</TABLE>";
			}
		}
	}else{
		foreach ( $service as $idService ) {
			$resultat = addUserService($idLogin, $idService);
			if ($resultat == 1 ) {
				$buffer .= "<TR><TD>ADD SERVICE $idService FOR $idLogin: [OK]</TD></TR>";		
			}else{
				$buffer .= "<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		    return $buffer ."</TABLE>";
			}
		}
		
		
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}

function displayDelUser() {
	
	$buffer = "<h1>Utilisateur(s) à supprimer: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delUser", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delUser\">";
    $buffer .= "<TABLE>";	
	
	
	$resultat = selectUser("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Utilisateurs</TD><TD><SELECT  NAME=\"selectUser[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idUser']."\">".$row['idUser']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
		$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	$buffer .= "</TABLE></FORM>";
	return $buffer ;
	
}


function displayResultDelUser($idLogin) {
	
	$buffer = "<h1>Utilisateur(s) à supprimer: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delUser", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $resultat = ctrlEmpty($idLogin, "Login Utilisateur");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}   
   foreach ($idLogin as $value) {
       $resultat = delUserDB($value);
	   $buffer .= "<TR><TD>$resultat</TD></TR>";  
   }
   $buffer .= "</TABLE>";
   return $buffer ;
}


function displayAddSwitch() {

	$buffer = "<h1>AJout d'un switch </h1>";
//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addSwitch", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}

    $buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addSwitch\">";
	$buffer .= "<TABLE>";
	
	$buffer .= "<TR><TD id=title>Nom du switch (ID): </TD><TD><SELECT NAME=\"selectIdSwitch\">";

	$filter = "(type = 'switch' OR type = 'routeur') AND hostname NOT IN (SELECT idSwitch FROM SWITCH) ; ";	
	//$filter = "type = 'switch'";
	$resultat = selectDevice($filter) ;
	if ( !is_string($resultat) ) {
		
	   while ( $rowDevice = mysql_fetch_assoc($resultat) ) {
				$buffer .= "<OPTION VALUE=\"" . $rowDevice['hostname'] . "\">" . $rowDevice['hostname'] . "</OPTION>" ;
		}
		
    }
	$buffer .= "</SELECT></TD></TR>";


	$buffer .= "<TR><TD id=title>Location: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textLocation\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id=title>Level: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textLevel\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id=title>Port à Ignorer: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textPortIgnore\"></INPUT></TD></TR>";
	
	//TODO verifier la cohérence du code pour l'affectation d'un ou plusieurs services à un switch
	$buffer .= "<TR><TD id=title>Service (choix multiple maintenir la touche ctrl): </TD>";
	
	
	$resultat = selectService("") ;
	//display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD><SELECT NAME=\"selectIdService[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idService']."\">".$row['idService']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	
	
	
	$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" NAME=\"AJOUTER\"></INPUT></TD></TR></TABLE></FORM>";
	
	return $buffer;

}

function displayResultAddSwitch($idSwitch, $location, $level, $portIgnore, $service) {
    
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addSwitch", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	
	$buffer .= "<TABLE>";
	
	$resultat = ctrlEmpty($idSwitch, "Nom du Switch");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	/*$resultat = ctrlIP($ip) ;
	if ( $resultat != 1 ) {
		return "<TR><TD>$resultat</TD></TR></TABLE>";
	}
	
	$resultat = ctrlEmpty($mac, "Adresse Mac");
	if ( $resultat != 1 ){
		return "<TR><TD>$resultat</TD></TR></TABLE>";
	}
	
	$resultat = ctrlMac($mac);
	if ( $resultat != 1 ){
		return "<TR><TD>$resultat</TD></TR></TABLE>";
	}*/
	
	$resultat = ctrlEmpty($service, "service du switch");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	
	$resultat = addSwitch($idSwitch, $location, $level, $portIgnore, $service);
	
	if ($resultat == 1 ) {
	   	$buffer .= "<TR><TD>CREATE SWITCH $idSwitch, $location, $level, $portIgnore: [OK]</TD></TR>";
	}else{
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	}
	
	//link switch to a service
	foreach ( $service as $idService) {
		
	   $resultat = addSwitchService($idSwitch, $idService);
		if ( $resultat == 1 ) {
			$buffer .= "<TR><TD>Switch $idSwitch linked to $idService: [OK]</TD</TR>";
		
        }else {
		    $buffer .= "<TR><TD>$resultat</TD></TR>";
		}
		
	 }
	$buffer .="</TABLE>";
	return $buffer;

}

function displayDelSwitch() {
	
	$buffer = "<h1>Switch(s) à supprimer: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delSwitch", "" );
	if ( $resultat != 1 ){
		$buffer = "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delSwitch\">";
	//$buffer = "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delSwitch\">";
	$buffer .= "<TABLE>";	
	
	
	$resultat = selectSwitch("") ;
	//display for debugger
    //echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Switch</TD><TD><SELECT  NAME=\"selectSwitch[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idSwitch']."\">".$row['idSwitch']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
		$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	$buffer .= "</TABLE></FORM>";
	return $buffer ;
}


function displayResultDelSwitch($idSwitch) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delSwitch", "$idSwitch" );
	if ( $resultat != 1 ){
		$buffer = "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $resultat = ctrlEmpty($idSwitch, "Nom du switch");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}

   foreach ($idSwitch as $value) {
       $resultat = delSwitch($value);
	   $buffer .= "<TR><TD>$resultat</TD></TR>";  
   }
   $buffer .= "</TABLE>";
   return $buffer;
}


function displayUpdateSwitch($idSwitch) {

	$buffer = "<h1>Mise à jour du switch $idSwitch </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "updateSwitch", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
    
    $buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=updateSwitch\">";
	$buffer .= "<TABLE>";
	
	
	$resultat = selectSwitch($idSwitch) ;
	
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		
	   while ( $row = mysql_fetch_assoc($resultat) ) {
	      		
		  $buffer .= "<TR><TD id=title>Nom du switch (ID): </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIdSwitch\" VALUE=\"".$row['idSwitch']."\"></INPUT></TD></TR>";
	      $buffer .= "<TR><TD id=title>IP du switch: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIP\" VALUE=\"".$row['ip']."\"></INPUT></TD></TR>";
		  $buffer .= "<TR><TD id=title>Adresse Mac du switch (ID): </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textMac\" VALUE=\"".$row['mac']."\"></INPUT></TD></TR>";
		  $buffer .= "<TR><TD id=title>Type: </TD><TD><SELECT NAME=\"selectType\">
					  <OPTION VALUE=\"switch\">Switch</OPTION>
					  <OPTION VALUE=\"router\">Router</OPTION></SELECT></TD></TR>";
		  $buffer .= "<TR><TD id=title>Location: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textLocation\" VALUE=\"".$row['location']."\"></INPUT></TD></TR>";
	      $buffer .= "<TR><TD id=title>Level: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textLevel\" VALUE=\"".$row['level']."\"></INPUT></TD></TR>";
	      $buffer .= "<TR><TD id=title>Port à Ignorer: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textportIgnore\" VALUE=\"".$row['portIgnore']."\"></INPUT></TD></TR>";
		
	   }
		$buffer .= "<TR><TD id='submit' COLSPAN=7><INPUT TYPE=\"SUBMIT\" VALUE=\"UPDATE\"></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	
	
	$buffer .= "<TR><TD id='submit'><SUBMIT NAME=\"UPDATE\"></TD></TR></TABLE></FORM>";
	
	return $buffer;

}


function displayResultUpdateSwitch($idSwitch, $ip, $mac, $type, $location, $level, $portIgnore) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "updateSwitch", "$idSwitch" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	
	$resultat = ctrlEmpty($idSwitch, "Nom du Switch");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	$resultat = ctrlEmpty($ip, "Adresse IP");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}

	$resultat = ctrlIP($ip) ;
        if ( $resultat != 1 ) {
		$buffer .="<TR><TD>$resultat</TD></TR>";
                $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
                return $buffer ;
        }
	
	$resultat = ctrlEmpty($mac, "Adresse Mac");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}

	$resultat = ctrlMac($mac);
        if ( $resultat != 1 ){
                $buffer .="<TR><TD>$resultat</TD></TR>";
                $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
                return $buffer ;
        }

	
	$resultat = updateSwitch($idSwitch, $location, $level, $portIgnore);
	
	
	if ($resultat == 1 ) {
	   	$buffer .= "<TR><TD>UPDATE SWITCH $idSwitch, $ip, $mac, $type, $location, $level, $portIgnore: [OK]</TD></TR>";
	}else{
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	}


        //update Device
        $resultat = updateDevice($idSwitch, $mac, $ip, $type);
        if ($resultat == 1 ) {
                $buffer .= "<TR><TD>UPDATE DEVICE $idSwitch, $ip, $mac, $type: [OK]</TD></TR>";
        }else{
                $buffer .= "<TR><TD>$resultat</TD></TR>";
                $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
        }



	$buffer .="</TABLE>";
	return $buffer;

}


function displayAllSwitch() {
	
	$buffer = "<h1>Liste des Switch</h1>";
   $buffer .= "<TABLE>";
   $buffer .= "<TR><TD id=title>Nom du switch (ID): </TD>
                              <TD id=title>IP</TD>
							  <TD id=title>Mac</TD>
							  <TD id=title>Type</TD>
                              <TD id=title>Location</TD>
							  <TD id=title>Level</TD>
							  <TD id=title>Port à Ignorer</TD>
                              <TD id=title>Service(s)</TD>
					  </TR>";
   $resultat = selectSwitch("") ;
	
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		
	   while ( $rowSwitch = mysql_fetch_assoc($resultat) ) {
			
			//select switch's services
			$service = "";
			
			$resService = selectSwitchService($rowSwitch['idSwitch'], "");
			//echo "type de ressources: " .get_resource_type($resService);
			 if ( !is_string($resService) ) {
				while ( $row = mysql_fetch_assoc($resService) ) {
				    $service .= $row['idService'] . " ";	
				}
			}
		    $buffer .= "<TR><TD id=klask><a href=\"./index.php?section=updateSwitch&switch=".$rowSwitch['idSwitch']."\">".$rowSwitch['idSwitch']."</a></TD>
							            <TD id=klask>".$rowSwitch['ip']."</TD>
                                        <TD id=klask>".$rowSwitch['mac']."</TD>
                                        <TD id=klask>".$rowSwitch['type']."</TD>
                                        <TD id=klask>".$rowSwitch['location']."</TD>
                                        <TD id=klask>".$rowSwitch['level']."</TD>
										<TD id=klask>".$rowSwitch['portignore']."</TD>
                                        <TD id=klask>".$service."</TD></TR>";
	   }
    }
	$buffer .= "</TABLE>";
	return $buffer ;
}



function displayAddService() {
	
	$buffer = "<h1>AJout d'un service</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addService", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addService\">";
	$buffer .= "<TABLE>";
	
	$buffer .= "<TR><TD id=title>Nom du service: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIdService\"></INPUT></TD></TR>";
	//$buffer .= "<TR><TD>Description: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textDescService\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" NAME=\"AJOUTER\"></TD></TR></TABLE></FORM>";
	
	return $buffer;
	
}

function displayResultAddService($idService) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addService", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	$resultat = ctrlEmpty($idService, "Service");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	$resultat = addService($idService);
	
	if ($resultat == 1 ) {
	   	$buffer .= "<TR><TD>CREATE SERVICE $idService: [OK]</TD></TR>";
	}else{
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	}

	$buffer .="</TABLE>";

	return $buffer;
}


function displayDelService() {
	
	$buffer = "<h1>Service(s) à supprimer: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delService", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delService\">";
    $buffer .= "<TABLE>";	
	
	
	$resultat = selectService("") ;
	//TODO display for debugger
    //echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Services</TD><TD><SELECT NAME=\"selectService[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idService']."\">". $row['idService']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
		$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	$buffer .= "</TABLE></FORM>";
	return $buffer ;
	
}

function displayResultDelService($service) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delSwitch", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";

   $resultat = ctrlEmpty($service, "Service");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}

   foreach ($service as $idService) {
       $resultat = delService($idService);
	   $buffer .= "<TR><TD>$resultat</TD></TR>";  
   }
   $buffer .= "</TABLE>";
   return $buffer ;
}


function displayAllService() {
	
   
	
   $resultat = selectService("") ;
	//TODO  to clean
	//echo "type de ressources: " .get_resource_type($resultat);
	$bufferSwitchService = "";
	$bufferUserService = "";
	if ( !is_string($resultat) ) {
		
	   while ( $row = mysql_fetch_assoc($resultat) ) {
			//SERVICE WITH USER
			//display checkbox delete for a classic authentification
				
			$resUserService = selectUserService("", $row['idService']);
			if ( !is_string($resUserService) ){
				//$user = "<TABLE>";
				$user = "";
				$checkDelUser = "";
				$bufferUserService .= "<TR><TD>" .  $row['idService'] . "</TD>";
				while ( $rowUserService = mysql_fetch_assoc($resUserService) ) {
					if ($GLOBALS['auth'] == "classic" ) {
						//$user .= "<TR><TD>" . $rowUserService['idUser'] . "</TD><TD><INPUT TYPE=\"CHECKBOX\" NAME=\"checkUserService[]\" VALUE=\"". $rowUserService['idUser'] . "@" . $row['idService'] . "\" ></TD></TR>";
						$user .= $rowUserService['idUser'] ."<BR />";
						$checkDelUser .= "<INPUT TYPE=\"CHECKBOX\" NAME=\"checkUserService[]\" VALUE=\"". $rowUserService['idUser'] . "@" . $row['idService'] . "\" ></INPUT><BR />";
					}else{
						//$user .= "<TR><TD>" . $rowUserService['idUser'] . "</TD></TR>";
						$user .= $rowUserService['idUser'] ."<BR />";
						
					}
				}
				//$user .= "</TABLE>";
				$bufferUserService .= "<TD>$user</TD><TD>$checkDelUser</TD></TR>" ;
			}else{
				$user = $resUserService ;
			}
			
			//SERVICE WITH SWITCH
			$resSwitchService = selectSwitchService("", $row['idService']);
			if ( !is_string($resSwitchService) ){
				//$switch = "<TABLE>";
				$switch = "";
				$checkDel = "";
				$bufferSwitchService .= "<TR><TD>" .  $row['idService'] . "</TD>";
				while ( $rowSwitchService = mysql_fetch_assoc($resSwitchService) ) {
					//$switch .= "<TR><TD>" . $rowSwitchService['idSwitch'] . "</TD><TD><INPUT TYPE=\"CHECKBOX\" NAME=\"checkSwitchService[]\" VALUE=\"". $rowSwitchService['idSwitch'] . "@" . $row['idService'] . "\" ></TD></TR></TR>";
					
					$switch .=  $rowSwitchService['idSwitch'] ."<BR />";
					
					$checkDel .= "<INPUT TYPE=\"CHECKBOX\" NAME=\"checkSwitchService[]\" VALUE=\"". $rowSwitchService['idSwitch'] . "@" . $row['idService'] . "\" ></INPUT><BR />";
				}
				
				//$switch .="</TABLE>";
				$bufferSwitchService .= "<TD>$switch</TD><TD>$checkDel</TD></TR>" ;
				
			}else{
					$switch = $resSwitchService ;
			}
			
			
		   
	   }
		
		$buffer = "<h1>Liste des Services par Utilisateurs</h1>";
		$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delUserSwitchService\"><TABLE>";
		$buffer .= "<TR><TD id=title>Nom du service: </TD><TD id=title>Utilisateurs</TD><TD id=title>Supprimer</TD></TR>";
		$buffer .= $bufferUserService ;
		$buffer .= "</TABLE>";
		
		$buffer .= "<h1>Liste des Services par Switch</h1>";
		$buffer .= "<TABLE>";
		$buffer .= "<TR><TD id=title>Nom du service: </TD><TD id=title>Switch</TD><TD id=title>Cocher pour supprimer</TD></TR>";
		$buffer .= $bufferSwitchService ;
		
			$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"VALIDER\"></INPUT></TD></TR>";
		$buffer .="</TABLE></FORM>";
		
   }

	return $buffer ;
	
}



function displayAddPower() {
	
	$buffer = "<h1>AJout de Droits</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addPower\">";
	$buffer .= "<TABLE>";
	
	$buffer .= "<TR><TD id=title>Nom du droit ou commande: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIdPower\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id=title>Description du droit ou commande: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textDescPower\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" NAME=\"AJOUTER\" VALUE=\"VALIDER\"></TD></TR></TABLE></FORM>";
	
	return $buffer;
	
}


function displayResultAddPower($idPower, $descPower) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	$resultat = ctrlEmpty($idPower, "Nom du droit ou de la commande");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$resultat = addPower($idPower, $descPower);

	if ($resultat == 1 ) {
	   	$buffer .= "<TR><TD>CREATE POWER $idPower: [OK]</TD></TR>";
	}else{
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	}
	$buffer .="</TABLE>";
	return $buffer;
}



function displayDelPower() {
	
	$buffer = "<h1>Droit(s) ou commande(s) à supprimer: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD= \"POST\" ACTION=\"./resultat.php\">";
    $buffer .= "<TABLE>";	
	
	
	$resultat = selectPower("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Droits</TD><TD><SELECT  NAME=\"selectPower[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idPower']."\">".$row['idPower']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
		$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	$buffer .= "</TABLE></FORM>";
	return $buffer ;	
}


function displayResultDelPower($power) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $resultat = ctrlEmpty($power, "Nom du droit ou de la commande");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}   
   foreach ($power as $idPower) {
       $resultat = delPower($idPower);
	   $buffer .= "<TR><TD>$resultat</TD></TR>";  
   }
   $buffer .= "</TABLE>";
   return $buffer ;
}



function displayAddUserPower()  {
	
	$buffer = "<h1>Ajout de Droits pour un Utilisateur: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addPowerUser\">";
	$buffer .= "<TABLE>";
	
	$resultat = selectUser("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Utilisateur</TD>";
		$buffer .= "<TD><SELECT NAME=\"selectUser[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"". $row['idUser']."\">".$row['idUser']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	
	$resultat = selectPower("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Droits ou Commandes</TD>";
		$buffer .= "<TD><SELECT NAME=\"selectPower[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idPower']."\">".$row['idPower']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	
	//$buffer .= "</TR>";
	$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"AJOUTER\"></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	
	return $buffer ;
	
}


function displayResultAddUserPower($user, $power) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	
	$buffer .= "<TABLE>";
	$resultat = ctrlEmpty($power, "Nom du droit ou de la commande");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$resultat = ctrlEmpty($user, "Login Utilisateur");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	foreach ( $user as $idLogin) {
		foreach ($power as $idPower) {
		   $resultat = addUserCMD($idLogin, $idPower);
		   if ($resultat == 1 ) {
	   	      $buffer .= "<TR><TD id=title>ADD POWER $idPower FOR $idLogin: [OK]</TD></TR>";
	       }else{
			  $buffer .= "<TR><TD>$resultat</TD></TR>";
		      //$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	        } 
		}   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}

function displayDelUserPower()  {
	
	$buffer = "<h1>Suppression de Droits: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delUserPower\">";
	$buffer .= "<TABLE>";
	
	$resultat = selectUser("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD><SELECT NAME=\"selectidLogin[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idLogin']."\">".$row['idLogin']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$resultat = selectPower("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD><SELECT NAME=\"selectPower[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idPower']."\">".$row['idPower']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$buffer .= "</TR>";
	$buffer .= "<TR><TD id='submit'><INPUT TYPE=\"SUBMIT\" VALUE=\"AJOUTER\"></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	
	return $buffer ;
	
}


function displayResultDelUserPower($user, $power) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer = "<TABLE>";
	$resultat = ctrlEmpty($power, "Nom du droit ou de la commande");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$resultat = ctrlEmpty($user, "Login Utilisateur");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	foreach ( $user as $idLogin) {
		foreach ($power as $idPower) {
		   $resultat = delUserCMD($idLogin, $idPower);
		   if ($resultat == 1 ) {
	   	      $buffer .= "<TR><TD>DEL POWER $idPower FOR $idLogin: [OK]</TD></TR>";
	       }else{
			  $buffer .= "<TR><TD>$resultat</TD></TR>";
		      $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	        } 
		}   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}



function displayPowerUser($idLogin, $idPower) {
	
	$buffer = "<h1>Liste des utilisateurs et de leurs droits </h1>";
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delPowerUserByAllPower\">";
	$buffer .= "<TABLE>";
	
	$buffer .= "<TR><TD id=title>UTILISATEUR</TD><TD id=title>DROITS</TD><TD id=title>cocher pour supprimer</TD></TR>";
	$resultat = selectUserCMD($idLogin, $idPower);
    if ( !is_string($resultat) ) {
		
	   while ( $row = mysql_fetch_assoc($resultat) ) {
          $buffer .= "<TR><TD>".$row['idUser']."</TD><TD>".$row['idPower']."</TD>";
		  $buffer .= "<TD> <INPUT TYPE=\"CHECKBOX\" NAME=\"checkPowerUser[]\" VALUE=\"". $row['idUser'] . "@" . $row['idPower'] . "\" ></TD></TR>";
	   }
	   
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	$buffer .= "<TR><TD id='submit'><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></INPUT></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	return $buffer ;
}

function displayResultDelUserPowerBydisplayAllPower($userPower) {
//check if user can do this command
	$buffer = "<h1>Résultat de la commande</h1>";
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	
	foreach ($userPower as $value) {
		
		$Tsplit = split("[@]", $value) ;
		$idLogin = $Tsplit[0] ;
		$idPower = $Tsplit[1];
		
		$resultat = ctrlEmpty($idPower, "Nom du droit ou de la commande");
		if ( $resultat != 1 ){
			$buffer .="<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
			return $buffer ;
		}
	
		$resultat = ctrlEmpty($idLogin, "Login Utilisateur");
		if ( $resultat != 1 ){
			$buffer .="<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
			return $buffer ;
		}
	
		$resultat = delUserCMD($idLogin, $idPower);
		if ($resultat == 1 ) {
			$buffer .= "<TR><TD>DEL POWER $idPower FOR $idLogin: [OK]</TD></TR>";
		}else{
			$buffer .= "<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		} 
		   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}


function displayPowerService($idService, $idPower) {
	$buffer = "<h1>Liste des droits par services</h1>";
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delPowerService\">";
	$buffer .= "<TABLE>";
	
	$buffer .= "<TR><TD id=title>SERVICE</TD><TD id=title>DROITS</TD></TR>";
	$resultat = selectCMDService($idService, $idPower);
    if ( !is_string($resultat) ) {
		
	   while ( $row = mysql_fetch_assoc($resultat) ) {
          $buffer .= "<TR><TD>".$row['idService']."</TD><TD>".$row['idPower']."</TD><TD><INPUT TYPE=\"checkbox\" NAME=\"checkPowerService[]\" VALUE=\"".$row['idService']."@".$row['idPower']."\"></INPUT></TD></TR>";
	   }
	   
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	$buffer .= "<TR><TD id='submit' COLSPAN=3><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></INPUT></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	return $buffer ;
}



function displayResultDelServicePower($servicePower) {
//check if user can do this command
	$buffer = "<h1>Résultat de la commande</h1>";
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	
	foreach ($servicePower as $value) {
		
		$Tsplit = split("[@]", $value) ;
		$idService = $Tsplit[0] ;
		$idPower = $Tsplit[1];
		
		$resultat = ctrlEmpty($idPower, "Nom du droit");
		if ( $resultat != 1 ){
			$buffer .="<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
			return $buffer ;
		}
	
		$resultat = ctrlEmpty($idService, "Nom du service");
		if ( $resultat != 1 ){
			$buffer .="<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
			return $buffer ;
		}
	
		$resultat = delCMDService($idService, $idPower) ;
		if ($resultat == 1 ) {
			$buffer .= "<TR><TD>DEL POWER $idPower FOR $idPower: [OK]</TD></TR>";
		}else{
			$buffer .= "<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		} 
		   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}




function displayPowerSwicth($idPower, $idLogin,$idSwitch) {
	$buffer = "<h1>Liste des droits utilisateurs par switch</h1>";
	$buffer .= "<TABLE>";
	$buffer .= "<TR><TD id=title>SWITCH</TD><TD id=title>USER</TD><TD id=title>Power</TD></TR>";
	$resultat = selectUserCMDSwitch($idPower, $idLogin,$idSwitch);
    if ( !is_string($resultat) ) {
		
	   while ( $row = mysql_fetch_assoc($resultat) ) {
          $buffer .= "<TR><TD>".$row['idSwitch']."</TD><TD>".$row['idLogin']."</TD><TD>".$row['idPower']."</TD></TR>";
	   }
	   
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	$buffer .= "</TABLE>";
	return $buffer ;

}


function displayAddPowerService()  {
	
	$buffer = "<h1>Ajout de droits ou commande(s) pour un service: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addPowerService\">";
	$buffer .= "<TABLE>";
	
	$resultat = selectService("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Service</TD><TD><SELECT NAME=\"selectService[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idService']."\">".$row['idService']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$resultat = selectPower("") ;
	//TODO echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD id=title>Droits</TD><TD><SELECT NAME=\"selectPower[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idPower']."\">".$row['idPower']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$buffer .= "</TR>";
	$buffer .= "<TR><TD id='submit'><INPUT TYPE=\"SUBMIT\" VALUE=\"AJOUTER\"></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	
	return $buffer ;
	
}



function displayResultAddPowerService($power, $service) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	
	$buffer .= "<TABLE>";
	$resultat = ctrlEmpty($power, "Nom du droit ou de la commande");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$resultat = ctrlEmpty($service, "Service");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	foreach ( $service as $idService) {
		foreach ($power as $idPower) {
		   $resultat = addCMDService($idService, $idPower);
		   if ($resultat == 1 ) {
	   	      $buffer .= "<TR><TD>ADD POWER $idPower FOR $idService: [OK]</TD></TR>";
	       }else{
			  $buffer .= "<TR><TD>$resultat</TD></TR>";
		     // $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	        } 
		}   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}


function displayDelPowerService()  {
	
	$buffer = "<h1>Suppression de droits pour un service: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delPowerService\">";
	$buffer .= "<TABLE>";
	
	$resultat = selectService("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD><SELECT NAME=\"selectService[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idService']."\">".$row['idService']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$resultat = selectPower("") ;
	//TODOdisplay for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD><SELECT NAME=\"selectPower[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idPower']."\">".$row['idPower']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$buffer .= "</TR>";
	$buffer .= "<TR><TD id='submit'><INPUT TYPE=\"SUBMIT\" VALUE=\"AJOUTER\"></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	
	return $buffer ;
	
}

function displayResultDelPowerService($power, $service) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delRight", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	$resultat = ctrlEmpty($power, "Nom du droit ou de la commande");
	if ($resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$resultat = ctrlEmpty($service, "Service");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	foreach ( $service as $idService) {
		foreach ($power as $idPower) {
		   $resultat = delCMDService($idService, $idPower);
		   if ($resultat == 1 ) {
	   	      $buffer .= "<TR><TD>DEL POWER $idPower FOR $idService: [OK]</TD></TR>";
	       }else{
			  $buffer .= "<TR><TD>$resultat</TD></TR>";
		      $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	        } 
		}   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}



function displayAddUserService()  {
	
	$buffer = "<h1>Ajout de Service pour un Utilisateur: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addServiceUser", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addUserService\">";
	$buffer .= "<TABLE>";
	
	$resultat = selectUser("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Utilisateur</TD><TD><SELECT NAME=\"selectUser[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idUser']."\">".$row['idUser']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$resultat = selectService("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD id=title>Service</TD><TD><SELECT NAME=\"selectService[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idService']."\">".$row['idService']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$buffer .= "</TR>";
	$buffer .= "<TR><TD COLSPAN=4 id='submit'><INPUT TYPE=\"SUBMIT\" VALUE=\"AJOUTER\"></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	
	return $buffer ;
	
}


function displayResultAddUserService($user, $service) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addServiceUser", "" );
	if ( $resultat != 1 ){
		$buffer.= "<TABLE>";
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	$resultat = ctrlEmpty($user, "Utilisateur");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$resultat = ctrlEmpty($service, "Service");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	foreach ( $service as $idService) {
		foreach ($user as $idLogin) {
		   $resultat = addUserService($idLogin, $idService);
		   if ($resultat == 1 ) {
	   	      $buffer .= "<TR><TD>ADD SERVICE $idService FOR $idLogin: [OK]</TD></TR>";
	       }else{
			  $buffer .= "<TR><TD>$resultat</TD></TR>";
		      $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	        } 
		}   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}


function displayAddSwitchService()  {
	
	$buffer = "<h1>Ajout de Service pour un Switch: </h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addServiceSwitch", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addSwitchService\">";
	$buffer .= "<TABLE>";
	
	$resultat = selectSwitch("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Switch</TD><TD><SELECT NAME=\"selectSwitch[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idSwitch']."\">".$row['idSwitch']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$resultat = selectService("") ;
	//TODO display for debugger
	//echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TD id=title>Service</TD><TD><SELECT  NAME=\"selectService[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idService']."\">".$row['idService']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD>";
	}else{
		$buffer .="<TD>$resultat</TD>";
	}
	
	$buffer .= "</TR>";
	$buffer .= "<TR><TD id='submit' COLSPAN=4><INPUT TYPE=\"SUBMIT\" VALUE=\"AJOUTER\"></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	
	return $buffer ;
	
}


function displayResultAddSwitchService($switch, $service) {
	
	$buffer = "<h1>Résultat de la commande</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addServiceSwitch", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	$resultat = ctrlEmpty($switch, "Switch");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return  $buffer ;
	}
	
	$resultat = ctrlEmpty($service, "Service");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	foreach ( $service as $idService) {
		foreach ($switch as $idSwitch) {
		   $resultat = addSwitchService($idSwitch, $idService);
		   if ($resultat == 1 ) {
	   	      $buffer .= "<TR><TD>ADD SERVICE $idService FOR $idSwitch: [OK]</TD></TR>";
	       }else{
			  $buffer .= "<TR><TD>$resultat</TD></TR>";
		      $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	        } 
		}   
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}

function displayDelUserSwitchService($userService, $switchService) {
	
	
	$buffer = "<TABLE>";
	
	if ( !empty($userService) ) {
		
		//del service's user
		foreach ( $userService as $value ) {
			$Tsplit = split("@", $value);
			echo "<TR><TD>" . delUserService($Tsplit[0], $Tsplit[1]) ."</TD></TR>";
		}
	}
	
	if (!empty($switchService) ) {
		
		//del service's switch
		foreach ( $switchService as $valueSwitch ) {
			$Tsplit = split("@", $valueSwitch) ;
			echo "<TR><TD>" . delSwitchService($Tsplit[0], $Tsplit[1]) ."</TD></TR>";
		}
	}
	$buffer .= "</TABLE>";
	
}



function displayAddNetwork() {
	
	$buffer = "<h1>AJout d'un réseau</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addNetwork", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addNetwork\">";
	$buffer .= "<TABLE>";
	
	$buffer .= "<TR><TD id=title>Adresse réseau (Syntaxe: 192.168.1.0/24): </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIdNetwork\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id=title>Nom du réseau: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textNameNetwork\"></INPUT></TD></TR>";
	
	$buffer .= "<TR><TD id=title>Interface réseau (Syntaxe: eth0): </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textInterface\" ></INPUT></TD></TR>";
	
	$buffer .= "<TR><TD id=title>Routeur</TD><TD><SELECT NAME=\"selectRouteur\">";
	
	$filter = "type='routeur'";
	$resultat = selectDevice($filter) ;
	if ( !is_string($resultat) ) {
		
	   while ( $row= mysql_fetch_assoc($resultat) ) {
			
		//	if ($rowSwitch['type'] == "router") {
				$buffer .= "<OPTION VALUE=\"" . $row['hostname'] . "\">" . $row['hostname'] . "</OPTION>" ;
			//}
		}
		
    }
	
	$buffer .= "</SELECT></TD></TR>";
	
	$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" NAME=\"AJOUTER\" VALUE=\"VALIDER\"></TD></TR></TABLE></FORM>";
	
	return $buffer;
	
}


function displayResultAddNetwork($addressNetwork, $nameNetwork, $interface,  $routeur) {
	
	$buffer = "<h1>Résultat de la commande </h1>";
	//TODO: penser à changer la commade par addNetwork
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addNetwork", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	
	//check is addressNetwork is correct
	$resultat = ctrlAddressNetwork($addressNetwork);
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	//check if nameNetwork is not empty
	$resultat = ctrlEmpty($nameNetwork, "Nom du Reseau");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	//check if interface is not empty
	$resultat = ctrlEmpty($interface, "Interface");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$resultat = ctrlEmpty($routeur, "Routeur");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$resultat = addNetwork($addressNetwork, $nameNetwork, $interface, $routeur) ;
	if ($resultat == 1 ) {
	   	$buffer .= "<TR><TD>CREATE NETWORK $addressNetwork $nameNetwork $interface $routeur : [OK]</TD></TR>";
		
	}else{
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		return $buffer ."</TABLE>";
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}


function displayAllNetwork() {
	
	$buffer = "<h1>Liste des Réseau</h1>";
	$buffer .= "<TABLE>";
	$buffer .= "<TR><TD id=title>Nom du réseau</TD><TD id=title>Réseau</TD><TD id=title>Interface</TD><TD id=title>Routeur<TD></TR>";
	$resultat = selectNetwork("") ;
	if ( !is_string($resultat) ) {
		
	   while ( $row = mysql_fetch_assoc($resultat) ) {
			
			$buffer .= "<TR><TD>" . $row['nameNetwork'] . "</TD><TD>" . $row['idNetwork'] . "</TD><TD>" . $row['interface'] . "</TD><TD>" . $row['mainRouter'] . "</TD></TR>";
			
		}
		
    }
	$buffer .= "</TABLE>";
	return $buffer ;
}


function displayDelNetwork() {
	
	$buffer = "<h1>Réseau(x) à supprimer: </h1>";
	//TODO Change droit to delNetwork
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delNetwork", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delNetwork\">";
	//$buffer = "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delSwitch\">";
	$buffer .= "<TABLE>";	
	
	
	$resultat = selectNetwork("") ;
	//display for debugger
    //echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Réseau</TD><TD><SELECT NAME=\"selectNetwork[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['idNetwork']."\">".$row['idNetwork']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
		$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	$buffer .= "</TABLE></FORM>";
	return $buffer ;
}


function displayResultDelNetwork($network) {
	
	$buffer = "<h1>Résultat de la commande </h1>";
	//TODO Change droit to delNetwork
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delNetwork", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $resultat = ctrlEmpty($network, "Nom du réseau");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}

   foreach ($network as $idNetwork) {
       $resultat = delNetwork($idNetwork);
	   $buffer .= "<TR><TD>$resultat</TD></TR>";  
   }
   $buffer .= "</TABLE>";
   return $buffer;
}



function displayAddDevice() {
	
	$buffer = "<h1>AJout d'un Device</h1>";
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addDevice", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR> </TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=addDevice\">";
	$buffer .= "<TABLE>";
	
	$buffer .= "<TR><TD id=title>Nom du device: </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textHostname\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id=title>Adresse Mac </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textMac\"></INPUT></TD></TR>";
	$buffer .= "<TR><TD id=title>Adresse IP </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIP\"></INPUT></TD></TR>";

	$buffer .= "<TR><TD id=title>Type</TD>";
	$buffer .= "<TD><SELECT  NAME=\"selectType\">";
	$buffer .= "<OPTION VALUE=\"switch\">switch</OPTION>";
	$buffer .= "<OPTION VALUE=\"routeur\">routeur</OPTION>";
	$buffer .= "<OPTION VALUE=\"wifi\">borne wifi</OPTION>";
	$buffer .= "<OPTION VALUE=\"workstation\">poste de travail</OPTION>";
	$buffer .= "<OPTION VALUE=\"server\">serveur</OPTION></SELECT></TD></TR>";
	
	$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" NAME=\"AJOUTER\" VALUE=\"VALIDER\"></TD></TR></TABLE></FORM>";
	
	return $buffer;
	
}


function displayResultAddDevice($hostname, $mac, $ip,  $type) {
	
	$buffer = "<h1>Résultat de la commande </h1>";
	//TODO: penser à changer la commade par addDevice
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "addDevice", "" );
	if ( $resultat != 1 ){
		$buffer.= "<TABLE>";
		$buffer .= "<TR><TD>" . $resultat ."</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<TABLE>";
	$resultat = ctrlIP($ip);
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	if ( ($type != "switch" and $type != "routeur" and $type != "wifi") or (!empty($mac) ) ) {
		
		$resultat = ctrlMac($mac);
		if ( $resultat != 1 ){
			$buffer .= "<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
			return $buffer ;
		}
	}
	
	$resultat = ctrlEmpty($hostname, "Nom du device");
	if ( $resultat != 1 ){
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	//ctrl if exist ip
	$filter = "hostname = '$hostname' ";
	$resultat = selectDevice($filter) ;
	if ( !is_string($resultat) ) {
		$row = mysql_fetch_assoc($resultat) ;
		if ($row['hostname'] == $hostname){
			$buffer .= "<TR><TD>ADD DEVICE WITH $hostname [FAILED]: hostname already exists</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
			return $buffer ;
		}
    }
	
	//ctrl if exist ip
	$filter = "ip = '$ip' ";
	$resultat = selectDevice($filter) ;
	if ( !is_string($resultat) ) {
		$row = mysql_fetch_assoc($resultat) ;
		if ($row['ip'] == $ip){
			$buffer .= "<TR><TD>ADD DEVICE WITH $ip [FAILED]: IP address already exists</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
			return $buffer ;
		}
    }
	
	//ctrl if mac exist
	if (!empty($mac) and $type != "routeur") {
		
		$filter = "mac = '$mac' ";
		$resultat = selectDevice($filter) ;
		if ( !is_string($resultat) ) {
			$row = mysql_fetch_assoc($resultat) ;
			if ($row['mac'] == $mac){
				$buffer .= "<TR><TD>ADD DEVICE WITH $mac [FAILED]: Mac address already exists</TD></TR>";
				$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
				return  $buffer ;
			}
		}
	}
	
	$resultat = addDevice($mac, $hostname, $ip, $type) ;
	if ($resultat == 1 ) {
		
	   	$buffer .= "<TR><TD>CREATE DEVICE $hostname $ip $mac $type : [OK]</TD></TR>";
		
	}else{
		$buffer .= "<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		return $buffer ."</TABLE>";
	}
	
	$buffer .="</TABLE>";
	return $buffer;
}



function displayAllDevice() {
	
	$buffer = "<h1>Liste des Devices</h1>";
	$buffer .= "<TABLE>";
	$buffer .= "<TR><TD id=title>Nom</TD><TD id=title>Adresse Mac</TD><TD id=title>Adresse IP</TD><TD id=title>Type<TD></TR>";
	$resultat = selectDevice("") ;
	if ( !is_string($resultat) ) {
		
	   while ( $row = mysql_fetch_assoc($resultat) ) {
			
			$buffer .= "<TR><TD>" . $row['hostname'] . "</TD><TD>" . $row['mac'] . "</TD><TD>" . $row['ip'] . "</TD><TD>" . $row['type'] . "</TD></TR>";
			
		}
		
    }
	$buffer .= "</TABLE>";
	return $buffer ;
}



function displayDelDevice() {
	
	$buffer = "<h1>Device(s) à supprimer: </h1>";
	//TODO Change droit to delDevice
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delDevice", "" );
	if ( $resultat != 1 ){
		$buffer = "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delDevice\">";
	//$buffer = "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=delSwitch\">";
	$buffer .= "<TABLE>";	
	
	
	$resultat = selectDevice("") ;
	//display for debugger
    //echo "type de ressources: " .get_resource_type($resultat);
	if ( !is_string($resultat) ) {
		$buffer .= "<TR><TD id=title>Devices</TD><TD><SELECT NAME=\"selectDevice[]\" MULTIPLE SIZE=5>";
	   while ( $row = mysql_fetch_assoc($resultat) ) {
         $buffer .= "<OPTION VALUE=\"".$row['hostname']."\">".$row['hostname']."</OPTION>";
	   }
	    $buffer .= "</SELECT></TD></TR>";
		$buffer .= "<TR><TD id='submit' COLSPAN=2><INPUT TYPE=\"SUBMIT\" VALUE=\"SUPPRIMER\"></TD></TR>";
	}else{
		$buffer .="<TD>$resultat</TD></TR>";
	}
	$buffer .= "</TABLE></FORM>";
	return $buffer ;
}


function displayResultDelDevice($device) {
	
	$buffer = "<h1>Resultat de la commande</h1>";
	//TODO Change droit to delDevice
	//check if user can do this command
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "delDevice", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $resultat = ctrlEmpty($device, "Nom du device");
	if ( $resultat != 1 ){
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}

   foreach ($device as $hostname) {
       $resultat = delDevice($hostname);
	   $buffer .= "<TR><TD>$resultat</TD></TR>";  
   }
   $buffer .= "</TABLE>";
   return $buffer;
}


function displayGenKlaskConf() {
	//TODO Change droit to delDevice
	//check if user can do this command
	$buffer = "<h1>Création du fichier klask.conf</h1>";
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "klaskexportdb", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $buffer .= "<TR><TD>" . genKlaskConf() ."</TD></TR></TABLE>";
	return $buffer ;
	
}

function displayKlaskExportdb() {
	$buffer = "<h1>Liste des postes de travail</h1>";
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "klaskexportdb", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE id=klask>";
   //$buffer .= "<TR><TD>";
   //$buffer .= "<TABLE width=10px>";
   $buffer .= klaskCommand("exportdb")  ."</TABLE>";
   //$buffer .= "</TD></TR>";
   $buffer .= "</TABLE>";
	return $buffer ;
	
}

function displayKlaskExportSw() {
	$buffer = "<h1>Liste des switch et leurs liaisions</h1>";
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "klaskexportsw", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $buffer .= "<TR><TD>" . klaskCommand("exportsw")  ."</TD></TR></TABLE>";
	return $buffer ;
	
}

function displayKlaskUpdateSw() {
	$buffer = "<h1>Mise à jour de la base switch</h1>";
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "klaskupdatesw", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $buffer .= "<TR><TD>" . klaskCommand("updatesw")  ."</TD></TR></TABLE>";
	return $buffer ;
	
}


function displayKlaskUpdateDb() {
	$buffer = "<h1>Mise à jour de la base switch</h1>";
	$resultat =0;
	$resultat = ctrlPower($_SESSION['login'],  "klaskupdatedb", "" );
	if ( $resultat != 1 ){
		$buffer .= "<TABLE>";
		$buffer .="<TR><TD>$resultat</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
		return $buffer ;
	}
	
   $buffer .= "<TABLE>";
   $buffer .= "<TR><TD>" . klaskCommand("updatedb")  ."</TD></TR></TABLE>";
	return $buffer ;
	
}

function displayKlaskGenMap() {
$buffer = "<h1>Carte du réseau</h1>";
$resultat = ctrlPower($_SESSION['login'],  "klaskgenmap", "" );
        if ( $resultat != 1 ){
                $buffer .= "<TABLE>";
                $buffer .="<TR><TD>$resultat</TD></TR>";
                $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
                return $buffer ;
        }
$buffer .= "<TABLE>";
$buffer .= "<TR><TD>" . klaskCommand("genmap") ."</TD></TR>";
$buffer .= "<TR><TD><a href=\"./klask/map.jpg\">Consulter la carte du réseau ici</a></TD></TR>";
$buffer .= "</TABLE>";
return $buffer ;
}



function displaySearch() {
	$buffer = "<h1>Recherche d'une machine</h1>";
	$buffer .= "<FORM METHOD=\"POST\" ACTION=\"./resultat.php?section=klaskSearch\">";
	
	$buffer .= "<TABLE id=\"Add\">";
	
	$buffer .= "<TR><TD id=title>Adresse IP de la machine </TD><TD><INPUT TYPE=\"TEXT\" NAME=\"textIP\"></INPUT></TD></TR>";	
	$buffer .= "<TR><TD COLSPAN=2><INPUT TYPE=SUBMIT NAME=\"RECHERCHER\" VALUE=\"RECHERCHER\"></INPUT></TD></TR>";
	$buffer .= "</TABLE></FORM>";
	
	echo $buffer ;
	
}


function displayKlaskSearch($ip) {
$buffer = "<h1>Recherche d'une machine</h1>";
$resultat = ctrlPower($_SESSION['login'],  "klasksearch", "" );
        if ( $resultat != 1 ){
                $buffer .= "<TABLE>";
                $buffer .="<TR><TD>$resultat</TD></TR>";
                $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR></TABLE>";
                return $buffer ;
        }
$buffer .= "<TABLE>";
$resultat = klaskSearch($ip);
if ( !empty($resultat )) {
	$buffer .= "<TR><TD>" . klaskSearch($ip) ."</TD></TR>";
}else{
	$buffer .= "<TR><TD>Aucun Résultat Trouvé</TD></TR>";
}
$buffer .= "</TABLE>";
return $buffer ;
}


?>
