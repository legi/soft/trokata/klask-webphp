<?

function ctrlAuth() {
	//session_start();
	if ((!isset($_SESSION['login'])) or (!isset($_SESSION['passwd']))) {
		
		return 0;
	}else{
		return 1;
	}
}

function auth($idLogin, $passwd) {

   if ( $idLogin == $GLOBALS['adminApp'] ) {
		$GLOBALS['auth'] = "classic" ;
	}

   if ( $GLOBALS['auth'] == "ldap" ) {
	  // must exist in database user
	  $resultat = selectUser($idLogin);
	  if ( !is_string($resultat) ) {
                while ( $row = mysql_fetch_assoc($resultat) ) {
                   if ( $row['idUser'] == $idLogin ) {
                      return auth_ldap($idLogin, $passwd) ;
		   }
		 }
                return "ACCESS DENIED FOR USER $idLogin";
           }
         
	   
	}else{
		if (  $GLOBALS['auth'] == "classic" ) {
			
			return  auth_user($idLogin, $passwd);
		}
	}
	
}

function ctrlEmpty($value, $saisie) {
	if (empty($value) ) {
		return "le champs $saisie est obligatoire";
	}else{
		return 1;
	}
}

function ctrlUnixAccount($idLogin) {
	$command='id '.$idLogin ;
	
	if ( exec($command) ) {
		return  1;
	}else{
		return 0;
	}	
}

function ctrlIP($ip){
	
	$Tsplit= split("[.]" , $ip ) ;

	if ( count($Tsplit) != 4 ) {
		return "Adresse IP non valide";
	}
	return 1;
}

function ctrlMac($mac) {
	$Tsplit = split("[:]", $mac);
	
	if ( count($Tsplit ) != 6 ) {
		return "Adresse MAC non valide";
	}
	
	return 1;
	
}


function ctrlAddressNetwork($addressNetwork) {
	
	$Tsplit = split("[/]", $addressNetwork); 
	if ( count($Tsplit) != 2) {
		return "Adresse Network non valide : netmask non saisie";
	}
	
	if ( !is_numeric($Tsplit[1]) ) {
		return "Adresse Network non valide : netmask non valide";
	}
	
	$TsplitIP= split("[.]" , $Tsplit[0] ) ;

	if ( count($TsplitIP) != 4 ) {
		return "Adresse Network non valide";
	}
	
	return 1;
}


      
?>
