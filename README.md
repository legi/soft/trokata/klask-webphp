# Klask-WebPHP / Interface Web en PHP pour Klask

Cette interface Web pour Klask est développée principalement par Kévin Reverchon, informaticien à l'ENS Lyon.
Elle est écrite en PHP et est sous licence libre CeCILL (compatible GPL).

Son développement suit au plus près celui de Klask et a été intégré par le passé dans le subversion de Klask dans une branche autonome du nom de web-klask.
N'ayant pas évolué depuis des années, il est à présent archivé sous forme de ce dépôt Git.

À noter qu'il y a dans le paquetage Klask une interface web minimal appellé ```push-web```.
L'interface dont il est question ici est bien plus complexe, programmé en PHP par des personnes de Lyon.
Le développeur principal de Klask n'utilise pas personnellement ce code et ne sais pas s'il fonctionne toujours...

L'interface Web à Klask est développé dans une branche autonome que l'on retrouve avec subversion à l'adresse

```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/klask-webphp.git
```
