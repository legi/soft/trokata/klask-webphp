<?
function genSectionEntete() {
	$content = "default:\n";
	$content .= "   community: public\n";
	$content .= "   snmpport: 161\n\n";

        $buffer = "<TR><TD id=title>Section Entête:</TD></TR>";
	$buffer .= "<TR><TD>default:<BR />";
	$buffer .= "community: public<BR />";
	$buffer .= "snmpport: 161</TD></TR>";
	
	echo $buffer ;
	
	return $content ;
	
}


function genSectionSwitch() {
	
	$content = "switch:\n";
        $buffer = "<TR><TD id=title>Section switch</TD></TR>";	
	$resultat = selectSwitch("") ;
	if ( !is_string($resultat) ) {
		
	   while ( $rowSwitch = mysql_fetch_assoc($resultat) ) {
			$content .= "   - hostname: " . $rowSwitch['idSwitch'] . "\n";
			$content .= "     location: " . $rowSwitch['location'] . "\n";
			$content .= "     level: " . $rowSwitch['level'] . "\n";
			$content .= "     portignore: \n";
			$content .= "\n\n\n";
		        $buffer .= "<TR><TD>".$rowSwitch['idSwitch']."<BR />"
                                             .$rowSwitch['location']."<BR />"
                                             .$rowSwitch['level']."<BR />"
					     .$rowSwitch['portignore']."</TD></TR>";
		}
		
    }
        echo $buffer ;
	return $content;
}

function genSectionNetwork() {
	$content = "network:\n";
        $buffer = "<TR><TD id=title>Section Netwotk</TD></TR>";	
	$resultat = selectNetwork("") ;
	if ( !is_string($resultat) ) {
		
		//TODO: attention je n'ai pas pris en compte dans la génération du fichier plusieurs plage reseau pour un nom reseau
	   while ( $rowNetwork = mysql_fetch_assoc($resultat) ) {
			$content .= "   " . $rowNetwork['nameNetwork'] . ":\n";
			$content .= "     ip-subnet:\n";
			$content .= "       - add: ".$rowNetwork['idNetwork'] . "\n";
			$content .= "     interface: " . $rowNetwork['interface'] . "\n";
			$content .= "     main-router: " . $rowNetwork['mainRouter'] . "\n";
			$content .= "\n\n\n";

			$buffer .="<TR><TD>".$rowNetwork['nameNetwork']."<BR />"
			          ."ip-subnet:<BR />"
				  ."- add: ".$rowNetwork['idNetwork']."<BR />"
				  ."interface: ".$rowNetwork['interface']."<BR />"
				  ."main-router: ".$rowNetwork['mainRouter'] ."</TD></TR>";
		}
    }
        echo $buffer ;
	
	return $content ;
}

function genSectionRouter() {
	
	$content = "router:\n";
	
	$filter = "type = 'routeur'";
	$resultat = selectDevice($filter) ;
	if ( !is_string($resultat) ) {
		
	   while ( $rowDev = mysql_fetch_assoc($resultat) ) {
			$content .= "   - hostname: " . $rowDev['hostname'] . "\n";
			$content .= "     mac-address: " . $rowDev['mac'] . "\n";
			$buffer .= "<TR><TD> - hostname: ".$rowDev['hostname']."<BR />"
			          ."mac-address: ".$rowDev['mac']."</TD></TR>";
		}
    }
        echo $buffer ;	
	$content .="\n\n";
	
	return $content ;
	
}



function genKlaskConf() {
	
	$fileKlask = "./klask/klask.conf";
	
	if (!$handle=fopen($fileKlask, "w+")) {
    $buffer = "Open file $fileKlask: [FAILED]";
	return $buffer;
}

	$content = genSectionEntete() ;
	$content .= genSectionNetwork() ;
	$content .= genSectionRouter() ;
	$content .= genSectionSwitch() ;

if (!fwrite($handle, $content)) {
    $buffer = "Write in file $fileKlask: [FAILED]";
	return $buffer;    
}

}

function klaskCommand($idRight) {
	$pathKlask = "sudo /usr/sbin/klask" ;
	
	$buffet = "";
	if ($idRight == "exportdb" ) {
	    $command=exec($pathKlask." ".$idRight , $ret) ;

		$cpt=0;
		$buffer =  "<TR><TD id=title>Switch</TD><TD id=title>Ports</TD><TD id=title>Hostname</TD><TD id=title>IP</TD><TD id=title>Mac</TD><TD id=title>Date</TD></TR>";
		foreach ($ret as $line) {
			$cpt++;
			if ($cpt > 2) {
				$buffer2 = str_replace('[ ]+', ' ' , trim($line));
				$Tsplit =  split('[ ]+', $buffer2);
				$buffer .=  "<TR><TD id='klask'>".trim($Tsplit[0]) ."</TD><TD id='klask'> ".  trim($Tsplit[1]) . "</TD><TD id='klask'>". trim($Tsplit[3]) . "</TD><TD id='klask'> " . trim($Tsplit[4]) ."</TD><TD id='klask'>" . trim($Tsplit[5]) . "</TD><TD id=klask>" . trim($Tsplit[6]) ." " . $Tsplit[7] ."</TD></TR>";
			}
		}
		return $buffer ;
		
	}else{
		
		if ($idRight == "updatesw") {
			
			$command=exec($pathKlask." ".$idRight , $ret) ;
			$buffer = "";
			 foreach ( $ret as $line ) {
				$buffer .= "$line <BR>";
			}
			return $buffer ;
		}else{
			if ($idRight == "updatedb") {
			
				$command=exec($pathKlask." ".$idRight , $ret) ;
				$buffer = "";
				foreach ( $ret as $line ) {
					$buffer = "$line <BR>";
				}
				return $buffer ;
			}else{
				if ($idRight == "exportsw") {
			
					$command=exec($pathKlask." ".$idRight , $ret) ;
					$buffer = "";
					foreach ( $ret as $line ) {
						if (fnmatch("Switch parent and children port inter-connection", $line)) {
							$buffer .= "<TR><TD COLSPAN=5>Switch parent and children port inter-connection</TD></TR>";
						}else{
							if ( fnmatch("Switch output port and parent port connection", $line)) {
								$buffer .= "<TR><TD COLSPAN=5>Switch output port and parent port connection</TD></TR>";
							}else{
								$buffer2 = str_replace('[ ]+', ' ' , trim($line));
								$buffer2 = str_replace('[<--+]', ' ' , trim($buffer2));
								$Tsplit =  split('[ ]+', $buffer2);
								$buffer .=  "<TR><TD id='klask'>".trim($Tsplit[0]) ."</TD><TD id='klask'> ".  trim($Tsplit[1]) . "</TD><TD id='klask'>". trim($Tsplit[3]) . "</TD><TD id=klask>". trim($Tsplit[4])."</TD></TR>";
							}
						}
					}
					return $buffer ;
				}else{
				   if ( $idRight == "genmap") {
						$command=exec($pathKlask." exportsw -f dot > ./klask/map.dot" , $ret) ;
						$command=exec("dot -Tjpg ./klask/map.dot > ./klask/map.jpg", $ret);
					}
				}
		    }
	} 
}
	
}


function KlaskSearch($ip) {
	$pathKlask = "sudo /usr/sbin/klask" ;
	$command=exec($pathKlask." search " ."$ip", $ret) ;
	$buffer = "";
	$cpt=0;
	foreach ( $ret as $line ) {
		$cpt++;
		if ( $cpt == 2 ) {
			if ( !empty($line) ) {
				$buffer2 = str_replace('[ ]+', ' ' , trim($line));
				$Tsplit =  split('[ ]+', $buffer2);
				$buffer = "<B>Nom: </B>" . $Tsplit[2] ."<BR />";
				$buffer .= "<B>IP: </B> " . $Tsplit[3] . "<BR />";
				$buffer .= "<B>Mac: </B> " . $Tsplit[4] . "<BR />";
				$buffer .= "<B>Connecté au Switch: </B>" . $Tsplit[0]. "<BR />";
				$buffer .= "<B>Port: </B>" . $Tsplit[1];
				return $buffer ;
			}else{
				return "" ;
			}
		}
	}
}


?>
