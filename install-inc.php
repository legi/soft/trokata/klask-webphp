<?

require('./libInstall.php');
require('./ctrl.php');
require('./libDisplay.php');

if ( empty($_GET['section'] )) {
	
   echo displaySaisieDB() ;
}

if ($_GET['section'] == "selectAuth" ) {
	
	echo displayChooseAuth() ;
}

if ( $_GET['section'] == "auth") {
	
	if ( $_POST['radioAuth'] == "ldap" ) {
		echo displaySaisieLDAP() ;
	}else{
			$buffer = "<h1>Fin de l'installation</h1>";
			$buffer .= "<TABLE>";
			$buffer .= "<TR><TD>Pré-requis pour l'application fonctionne</TD></TR>";
			$buffer .= "<TR><TD> - klask fonctionnel en ligne de commande</TD></TR>";
			$buffer .= "<TR><TD> - klask doit être configuré avec le bon path pour le fichier klask.conf /var/www/guiKlask/klask/klask.conf</TD></TR>";
			$buffer .= "<TR><TD> - Utilisateur www-data dans le fichier sudoer UNIQUEMENT pour la commande /usr/sbin/klask</TD></TR>";
			$buffer .= "<TR><TD><a href=\"./index.php\">Poursuivre</a></TD></TR></TABLE>";
			echo $buffer ;
	}
	
}




if ( $_GET['section'] == "saisieDB" ) {
	
	$buffer = "<h1>Installation: Résultat de la saisie des paramètres</h1>";
	$buffer .= "<TABLE>";
	if ( empty($_POST['textHostDB']) or empty($_POST['textNameDB']) or empty($_POST['textUserCreateDB']) or empty($_POST['textPasswdCreateDB']) or empty($_POST['textUserDB' ]) or empty($_POST['textPasswdDB']) or empty($_POST['textAdminApp']) or empty($_POST['textPasswdAdminApp']) ) {
		
		$buffer .= "<TR><TD>Les champs hôte, nom de la base, utilisateur autorisé à créer une base, mot de passe de l'utilisateur autorisé, nom de lutilisateur pour l'accès de l'application à la base de données, et le mot de passe de l'utilisateur pour l'accès à la base de données sont OBLIGATOIRES.</TD></TR> ";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		
    }else{
	    
       $resultat = createDatabase($_POST['textHostDB'], $_POST['textNameDB'], $_POST['textUserCreateDB'], $_POST['textPasswdCreateDB'], $_POST['textUserDB'], $_POST['textPasswdDB'] , $_POST['textAdminApp'], $_POST['textPasswdAdminApp']);

	   if ( $resultat == 1 ) {
		  
		  $buffer .= "<TR><TD>CREATE DATABASE: [OK]</TD></TR>";
		  $buffer .= "<TR><TD>CREATE ACCESS DATABASE FOR " . $_POST['textUserDB'] .": [OK]</TD></TR>";
		  $buffer .= "<TR><TD>CREATE TABLE USER: [OK]</TD></TR>";
		  $buffer .= "<TR><TD>CREATE TABLE SERVICE: [OK]</TD></TR>";  
		  $buffer .= "<TR><TD>CREATE TABLE SERVICEUSER: [OK]</TD></TR>"; 
		  $buffer .= "<TR><TD>CREATE TABLE POWER: [OK]</TD></TR>";  
		  $buffer .= "<TR><TD>CREATE TABLE CMDUSER: [OK]</TD></TR>";
		  $buffer .= "<TR><TD>CREATE TABLE CMDSERVICE: [OK]</TD></TR>";
		  $buffer .= "<TR><TD>CREATE TABLE SWITCH: [OK]</TD></TR>";  
		  $buffer .= "<TR><TD>CREATE TABLE CMDSWITCH: [OK]</TD></TR>";
		  $buffer .= "<TR><TD>CREATE TABLE SERVICESWITCH: [OK]</TD></TR>";
		$buffer .= "<TR><TD>CREATE TABLE NETWORK: [OK]</TD></TR>";
		$buffer .= "<TR><TD>CREATE TABLE DEVICE: [OK]</TD></TR>";
		  $buffer .= "<TR><TD>INSERT POWER FOR TABLE SERVICESWITCH: [OK]</TD></TR>";  
		  $buffer .= "<TR><TD>INSERT ADMIN ACCOUNT FOR TABLE USER: [OK]</TD></TR>";    
		  
		  $resultat = createFileConfig($_POST['textHostDB'], $_POST['textNameDB'], $_POST['textUserDB'], $_POST['textPasswdDB'], $_POST['textAdminApp']);
		
		  if ( $resultat == 1 ) {
			$buffer .="<TR><TD>CREATE FILE ./conf/config.php: [OK]</TD></TR>";
   		  }else{
			  $buffer .= "<TR><TD>$resultat</TD></TR>" ;
			  $buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		  }
		
		$buffer .= "<TR><TD><a href=\"./install.php?section=selectAuth\">Etape suivante</a></TD></TR>";  
		
		}else{
			$buffer .= "<TR><TD>$resultat</TD></TR>" ;
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
		}

   }
	
	$buffer .= "</TABLE>";
	echo $buffer ;
	
}

  if ( $_GET['section'] == "createFileAuth" ) {
	
	$buffer ="<h1>Installation: Résultat de la  saisie des parmètres</h1>";
	$buffer .= "<TABLE>";
	if ( empty($_POST['textHostLdap']) or empty($_POST['textBindUser']) or empty($_POST['textService'] ) ) {
		
		$buffer .= "<TR><TD>Les champs Nom du serveur LDAP, Bind des Utilisateurs, champs du service d'affectation de l'utilisateur sont obligatoires</TD></TR>";
		$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
	}else{
		
		$resultat = createFileLDAP($_POST['textHostLdap'], $_POST['textBindUser'], $_POST['textService']) ;
		if ( $resultat == 1 ) {
			$buffer .= "<TR><TD>Fin de l'installation</TD></TR>";
			$buffer .= "<TR><TD>Pré-requis pour l'application fonctionne</TD></TR>";
			$buffer .= "<TR><TD> - klask fonctionnel en ligne de commande</TD></TR>";
			$buffer .= "<TR><TD> - klask doit être configuré avec le bon path pour le fichier klask.conf /var/www/guiKlask/klask/klask.conf</TD></TR>";
			$buffer .= "<TR><TD> - Utilisateur www-data dans le fichier sudoer UNIQUEMENT pour la commande /usr/sbin/klask</TD></TR>";
			$buffer .= "<TR><TD><a href=\"./index.php\">Poursuivre</a></TD></TR>";
			$buffer .= "<TR><TD>CREATE FILE ./ldap/auth_ldap.php : [OK]</TD></TR>";
			
		}else{
			
			$buffer .= "<TR><TD>$resultat</TD></TR>";
			$buffer .= "<TR><TD><a href=\"javascript:history.back()\">page précédente</a></TD></TR>";
			
		}
		
	}
	
	$buffer .= "</TABLE>";
	
	echo $buffer;
	
}

	
function displaySaisieInstall() {
	
	//Nom de la base
	//Nom de l'utilisateur pour la création de la base
	//Password de l'utilisateur pour la création de la base
	//Création de la base
	
	//Demande si c'est une auth LDAP ou mysql avec comme contrainte d'exister dans le /etc/passwd
	
}











?>