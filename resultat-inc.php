<?
//session_start();
require('./libInstall.php');
require('./libDisplay.php');
require('./ctrl.php');



if ( $_GET['section'] == "addService" ) {
	if ( $GLOBALS['auth'] == "classic" ) {
		echo displayResultAddService($_POST['textIdService']);	
	}else{
		echo "<h1>Résultat de la Commande</h1>";
		echo "ADD SERVICE [FAILED]: Your authentification use LDAP<BR>"; 
	}
	
	
}

if ( $_GET['section'] == "addUser" ) {
	if ( $GLOBALS['auth'] == "classic" ) {
		echo displayResultAddUser($_POST['textIdLogin'], $_POST['textPasswdUser'], $_POST['selectIdService'] );	
	}else{
		echo displayResultAddUser($_POST['textIdLogin'], "", "");
	}
}

if ( $_GET['section'] == "addSwitch" ) {
	echo displayResultAddSwitch($_POST['selectIdSwitch'], $_POST['textLocation'], $_POST['textLevel'], $_POST['textPortIgnore'], $_POST['selectIdService']);	
	
}

if ( $_GET['section'] == "addPower" ) {
	echo displayResultAddPower($_POST['textIdPower'], $_POST['textDescPower']);	
}

if ( $_GET['section'] == "addPowerUser" ) {
	echo displayResultAddUserPower($_POST['selectUser'], $_POST['selectPower']);	
}

if ( $_GET['section'] == "addUserService" ) {
	
	if ( $GLOBALS['auth'] == "classic" ) {
		echo displayResultAddUserService($_POST['selectUser'], $_POST['selectService']);	
	}else{
		echo "<h1>Résultat de la Commande</h1>";
		echo "ADD SERVICE FOR USER [FAILED]: Your authentification use LDAP<BR>"; 
	}
}

if ( $_GET['section'] == "addSwitchService" ) {
	echo displayResultAddSwitchService($_POST['selectSwitch'], $_POST['selectService']);
}

if ( $_GET['section'] == "addPowerService" ) {
	echo displayResultAddPowerService($_POST['selectPower'], $_POST['selectService']);	
}

if ( $_GET['section'] == "delUser" ) {
	echo displayResultDelUser($_POST['selectUser']);	
	
}

if ( $_GET['section'] == "delService" ) {
	if ( $GLOBALS['auth'] == "classic" ) {
		echo displayResultDelService($_POST['selectService']);	
	}else{
		echo "<h1>Résultat de la Commande</h1>";
		echo "DELETE SERVICE [FAILED]: Your authentification use LDAP<BR>"; 
	}
	
}

if ( $_GET['section'] == "delSwitch" ) {
	echo displayResultDelSwitch($_POST['selectSwitch']);	
}

if ( $_GET['section'] == "delUserSwitchService" ) {
	
	echo displayDelUserSwitchService($_POST['checkUserService'], $_POST['checkSwitchService']);	
}


if ( $_GET['section'] == "addDevice" ) {
	echo displayResultAddDevice($_POST['textHostname'], $_POST['textMac'], $_POST['textIP'], $_POST['selectType']);	
}

if ( $_GET['section'] == "addNetwork" ) {
	echo displayResultAddNetwork($_POST['textIdNetwork'], $_POST['textNameNetwork'], $_POST['textInterface'], $_POST['selectRouteur']);	
}

if ( $_GET['section'] == "delDevice" ) {
	echo displayResultDelDevice($_POST['selectDevice']);	
}

if ( $_GET['section'] == "delNetwork" ) {
	echo displayResultDelNetwork($_POST['selectNetwork']);	
}

if ( $_GET['section'] == "delPowerUserByAllPower" ) {
	echo displayResultDelUserPowerBydisplayAllPower($_POST['checkPowerUser']);	
}

if ( $_GET['section'] == "delPowerService" ) {
	echo displayResultDelServicePower($_POST['checkPowerService']);
}

if ( $_GET['section'] == "updateSwitch" ) {
	echo displayResultUpdateSwitch($_POST['textIdSwitch'], $_POST['textIP'], $_POST['textMac'], $_POST['selectType'], $_POST['textLocation'], $_POST['textLevel'], $_POST['textportIgnore'] );
}

if ( $_GET['section'] == "klaskSearch" ) {
	echo displayKlaskSearch($_POST['textIP']);
}

?>
